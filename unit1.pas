unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    ClearDice: TButton;
    G1: TButton;
    G2: TButton;
    G3: TButton;
    G4: TButton;
    G5: TButton;
    G6: TButton;
    BlackDie: TButton;
    GB1: TGroupBox;
    LG1: TLabel;
    LG2: TLabel;
    LG3: TLabel;
    LG4: TLabel;
    LG5: TLabel;
    LG6: TLabel;
    LBlackDie: TLabel;
    Splitter1: TSplitter;
    procedure BlackDieClick(Sender: TObject);
    procedure ClearDiceClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure G1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure G2Click(Sender: TObject);
    procedure G3Click(Sender: TObject);
    procedure G4Click(Sender: TObject);
    procedure G5Click(Sender: TObject);
    procedure G6Click(Sender: TObject);
    function dice (num, skip:integer; Sender:TObject):string;
    procedure RxDice1Change(Sender: TObject);

  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);

begin
  randomize;

end;

procedure TForm1.G1Click(Sender: TObject);
var i:integer;
begin
  //1-2
     G1.Caption:=dice(2,1,G1); //0..1
end;

procedure TForm1.BlackDieClick(Sender: TObject);
begin
  //1-20
  BlackDie.Caption:=dice(20,1,BlackDie); // 0..19
end;

procedure TForm1.ClearDiceClick(Sender: TObject);
begin
   G1.Caption:='X';
   G2.Caption:='X';
   G3.Caption:='X';
   G4.Caption:='X';
   G5.Caption:='X';
   G6.Caption:='X';
   BlackDie.Caption:='X';

end;



procedure TForm1.G2Click(Sender: TObject);
begin
  //2-4
  G2.Caption:=dice(3,2, G2);// 0..2
end;

procedure TForm1.G3Click(Sender: TObject);
begin
  //4-8 (4 5 6 7 8)
  G3.Caption:=dice(5,4, G3);// 0..4
end;

procedure TForm1.G4Click(Sender: TObject);
begin
  //7-12 (7 8 9 10 11 12)
  G4.Caption:=dice(6,7, G4);// 0..5
end;

procedure TForm1.G5Click(Sender: TObject);
begin
  //11..20
  G5.Caption:=dice(10,11,G5);// 0..9
end;

procedure TForm1.G6Click(Sender: TObject);
begin
  //21..30
  G6.Caption:=dice(10,21, G6);// 0..9
end;

function TForm1.dice (num, skip:integer; Sender: TObject):string;
var i:integer;
begin
    for i := random(5)+5 to random(20)+20 do
     begin

     //1-2
        dice:=inttostr(random(num)+skip); //0..1
        sleep(50);
        TButton(Sender).Caption:=dice;
     end;
end;

procedure TForm1.RxDice1Change(Sender: TObject);
begin

end;


procedure TForm1.FormResize(Sender: TObject);
var cl_h, cl_w, col2l, col3l, row2t, row3t:integer;
    b_w, b_h, marg_l, marg_t, space_w, space_h:integer;
    space_l_h:integer;

begin


  cl_w:= GB1.ClientWidth;
  cl_h:= GB1.ClientHeight;
//  Caption:=inttostr(cl_w)+' ' +inttostr(cl_h);

  marg_l:=5;
  marg_t:=15;

  space_w:=marg_l*2;
  space_h:=marg_t*3;     // 60

  b_w:=(cl_w  - space_w*2 - marg_l*2) div 3;
//         360         120        60
  b_h:=(cl_h  - space_h*2 - marg_t*2 ) div 3;


  col2l:=marg_l+b_w+space_w;
  col3l:=marg_l+b_w+space_w+b_w+space_w;

  g1.Left:=marg_l;   g2.Left:=col2l;  g3.Left:=col3l;
  g4.Left:=marg_l;   g5.Left:=col2l;  g6.Left:=col3l;
                    BlackDie.Left:=col2l;  ClearDice.Left:=col3l;

  Lg1.Left:=marg_l+5;   Lg2.Left:=col2l+5;  Lg3.Left:=col3l+5;
  Lg4.Left:=marg_l+5;   Lg5.Left:=col2l+5;  Lg6.Left:=col3l+5;
                    LBlackDie.Left:=col2l+5;



  row2t:=marg_t+b_h+space_h;
  row3t:=marg_t+b_h+space_h+b_h+space_h;

  g1.Top:=marg_t;     g2.Top:=marg_t;  g3.Top:=marg_t;
  g4.Top:=row2t;      g5.Top:=row2t;   g6.Top:=row2t;
                    BlackDie.Top:=row3t;   ClearDice.Top:=row3t;

  space_l_h:=(marg_t div 1)+2;
  Lg1.Top:=marg_t-space_l_h;     Lg2.Top:=marg_t-space_l_h;  Lg3.Top:=marg_t-space_l_h;
  Lg4.Top:=row2t-space_l_h;      Lg5.Top:=row2t-space_l_h;   Lg6.Top:=row2t-space_l_h;
                    LBlackDie.Top:=row3t-space_l_h;

  g1.Width:=b_w; g2.Width:=b_w; g3.Width:=b_w;
  g4.Width:=b_w; g5.Width:=b_w; g6.Width:=b_w;
                    BlackDie.Width:=b_w;  ClearDice.Width:=b_w;

  g1.Height:=b_h; g2.Height:=b_h; g3.Height:=b_h;
  g4.Height:=b_h; g5.Height:=b_h; g6.Height:=b_h;
                    BlackDie.Height:=b_h;  ClearDice.Height:=b_h;

  g1.Font.Size:=b_h div 2;
  g2.Font.Size:=b_h div 2;
  g3.Font.Size:=b_h div 2;
  g4.Font.Size:=b_h div 2;
  g5.Font.Size:=b_h div 2;
  g6.Font.Size:=b_h div 2;
  BlackDie.Font.Size:=b_h div 2;
  ClearDice.Font.Size:=b_h div 2;

  Lg1.Font.Size:=space_h div 4;
  Lg2.Font.Size:=space_h div 4;
  Lg3.Font.Size:=space_h div 4;
  Lg4.Font.Size:=space_h div 4;
  Lg5.Font.Size:=space_h div 4;
  Lg6.Font.Size:=space_h div 4;
  LBlackDie.Font.Size:=space_h div 4;


  //form1.Constraints.MinHeight:=GB1.Constraints.MinHeight;
  //form1.Constraints.MinWidth:=GB1.Constraints.MinWidth;

  //g1.
end;

end.


